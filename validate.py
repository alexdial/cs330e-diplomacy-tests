import glob
import os.path
import sys

files = glob.glob("*")
for f in files:
  _, ext = os.path.splitext(f)
  if ext not in [".out", ".in", ".py", ".md"]:
    print("Invalid file name ", f)
    sys.exit(1)

