#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve(self):
        r = StringIO("A Austin Move Phoenix\nB Denver Hold\nC Phoenix Support D\nD Tallahassee Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Denver\nC [dead]\nD Tallahassee\n"
        )

    def test_solve_1(self):
        r = StringIO("A Miami Move Orlando\nB Orlando Move Miami\nC Tallahassee Move Gainesville\nD Tampa Hold\nE Gainesville Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Orlando\nB Miami\nC [dead]\nD Tampa\nE [dead]\n"
        )

    def test_solve_2(self):
        r = StringIO("A Dallas Hold\nB Houston Move Dallas\nC Austin Move Galveston\nD Galveston Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        )

    def test_solve_3(self):
        r = StringIO("A Dallas Move Austin\nB Houston Move Dallas\nC Austin Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Dallas\nC Houston\n"
        )

    def test_solve_4(self):
        r = StringIO("A Pflugerville Hold\nB Bastrop Move Pflugerville\nC Leander Move Pflugerville")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n"
        )

    def test_solve_5(self):
        r = StringIO("A Texas Hold\nB California Move Texas\nC Florida Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Texas\nB [dead]\nC Florida\n"
        )

    def test_solve_6(self):
        r = StringIO("X Texas Support A\nW Washington Hold\nA Alaska Move Washington")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "X Texas\nW [dead]\nA Washington\n"
        )
    
    def test_solve_7(self):
        r = StringIO("A Texas Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Texas\n"
        )

    def test_solve_8(self):
        r = StringIO("A Texas Move London\nB California Move Texas\nC Florida Move Texas")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB [dead]\nC [dead]\n"
        )

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
"""